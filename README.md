# videoloop-deploy
Bits of code to play video files from `/home/pi/drive/samples` in a loop on the Raspberry Pi using omxplayer.
Starts playing on boot and makes sure the loop is running every 5 minutes.
Reboots every night at 6AM.

## install.sh
This file will deploy the solution on the host. Please use `sudo ./install.sh` to run the script.