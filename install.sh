# VIDEOLOOP v0.3 install script
## Default path for video folder is /home/pi/drive/samples

## Install dependencies, remove uneeded stuff
apt-get remove wolfram* -y
apt-get remove libreoffice* -y
apt-get update -y
apt-get install omxplayer -y
apt-get install screen -y

## Deploy videoloop script
cp videoloop.sh /home/pi/videoloop.sh
chmod 755 /home/pi/videoloop.sh

## Deploy the controller
cp vlcontroller.sh /etc/init.d/vlcontroller
chmod 755 /etc/init.d/vlcontroller

## Start videoloop on boot
update-rc.d vlcontroller defaults

## Blanks the background of the videoloop after 2 seconds
sed -i.bck 's/$/ consoleblank=2/' /boot/cmdline.txt

## Add operations to crontab
echo "0 6 * * * sudo reboot" >> tempfile
### Check if loop is running every 5 minutes and restarts it if needed
echo "*/5 * * * * /etc/init.d/vlcontroller repair" >> tempfile
crontab tempfile
rm tempfile

## Apply upgrade everything
apt autoremove
apt-get upgrade -y